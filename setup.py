import setuptools

with open("README.md") as fh:
    long_description=fh.read()


setuptools.setup(
    name="trinixsosmedgraph",
    version="0.0.1",
    # scripts=[],
    author="asri djufri",
    author_email="asri@trinix.id",
    description="trinixsosmedgraph",
    long_description = long_description,
    url="https://bitbucket.org/asrijuhas/digivlasna",
    packages=setuptools.find_packages(),
    install_requires=[
        'urllib3<1.25,>=1.23',
        'py2neo'
    ],
    classifiers=[
        "Program language : python : 3.7 ",
        "Project: digivla sna"
    ]
)