from py2neo.ogm import GraphObject, Property, RelatedFrom, RelatedTo

class Country(GraphObject):
    __primarykey__ = "code"
    code = Property()
    name = Property()

class Location(GraphObject):
    __primarykey__ = "name"
    name = Property()
    parent_location = RelatedTo("Location")

class HashTag(GraphObject):
    __primarykey__ = "text"
    text = Property()

class Keyword(GraphObject):
    __primarykey__ = "text"
    text = Property()

class Topic(GraphObject):
    __primarykey__ = "topic_id"
    topic_id = Property()
    text = Property()
    topic_keyword = RelatedTo(Keyword)


class Person(GraphObject):
    __primarykey__ = "person_id"
    person_id = Property()
    screen_name = Property()
    name = Property()
    created_at = Property()
    followers_count = Property()
    likes_count = Property()
    friends_count = Property()
    post_count = Property()
    knows = RelatedTo("Person")



class Post(GraphObject):
    __primarykey__ = "post_id"
    post_id = Property()
    platform = Property()
    text = Property()
    created_at = Property()
    reshared_count = Property()
    comment_count = Property()
    likes_count = Property()
    view_count = Property() 
    post_type = Property()
    hashtags = RelatedTo(HashTag)
    has_keywords = RelatedTo(Keyword)
    posted_by = RelatedTo(Person)
    # reshared_by = RelatedTo(Person)
    
    mentions = RelatedTo(Person)
    in_country = RelatedTo(Country)
    in_location = RelatedTo(Location)
    shared_post = RelatedTo("Post")
    commented_post = RelatedTo("Post")




